<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:xs="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsl-="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                exclude-result-prefixes="xlink mml">


    <!--<xsl:output doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"-->
                <!--doctype-system="http://www.w3.org/TR/html4/loose.dtd"-->
                <!--encoding="UTF-8"/>-->

    <xsl:output method="html"
        encoding="UTF-8" indent="no"/>

    <xsl:strip-space elements="*"/>

    <!-- Space is preserved in all elements allowing #PCDATA -->
    <!--<xsl:preserve-space-->
            <!--elements="abbrev abbrev-journal-title access-date addr-line-->
              <!--mml:annotation mml:ci mml:cn mml:csymbol mml:mi mml:mn-->
              <!--mml:mo mml:ms mml:mtext"/>-->


    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>


    <xsl:template match="/">
        <div class="wrap">
            <a role="button" onclick="translatePage('fr')">FR</a> | <a role="button" onclick="translatePage('en')">EN</a>
            <div id="mode-color-btns">
                <button id="sun" onclick="darkMode(true)">
                    <svg style="" class="icon" focusable="false" viewBox="0 0 32 32">
                        <path d="M16 12.005a4 4 0 1 1-4 4a4.005 4.005 0 0 1 4-4m0-2a6 6 0 1 0 6 6a6 6 0 0 0-6-6z"
                              fill="#adbac7"></path>
                        <path d="M5.394 6.813l1.414-1.415l3.506 3.506L8.9 10.318z" fill="#adbac7"></path>
                        <path d="M2 15.005h5v2H2z" fill="#adbac7"></path>
                        <path d="M5.394 25.197L8.9 21.691l1.414 1.415l-3.506 3.505z" fill="#adbac7"></path>
                        <path d="M15 25.005h2v5h-2z" fill="#adbac7"></path>
                        <path d="M21.687 23.106l1.414-1.415l3.506 3.506l-1.414 1.414z" fill="#adbac7"></path>
                        <path d="M25 15.005h5v2h-5z" fill="#adbac7"></path>
                        <path d="M21.687 8.904l3.506-3.506l1.414 1.415l-3.506 3.505z" fill="#adbac7"></path>
                        <path d="M15 2.005h2v5h-2z" fill="#adbac7"></path>
                    </svg>
                </button>
                <button id="moon" onclick="darkMode(false)" style="display:none;">
                    <svg style="" class="icon" focusable="false" viewBox="0 0 32 32">
                        <path d="M13.502 5.414a15.075 15.075 0 0 0 11.594 18.194a11.113 11.113 0 0 1-7.975 3.39c-.138
                    0-.278.005-.418 0a11.094 11.094 0 0 1-3.2-21.584M14.98 3a1.002 1.002 0 0 0-.175.016a13.096 13.096
                    0 0 0 1.825 25.981c.164.006.328 0 .49 0a13.072 13.072 0 0 0 10.703-5.555a1.01 1.01 0 0
                    0-.783-1.565A13.08 13.08 0 0 1 15.89 4.38A1.015 1.015 0 0 0 14.98 3z" fill="#adbac7">
                        </path>
                    </svg>
                </button>
            </div>

            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:TEI">
        <div class="article-and-metas">
            <div class="left-contents" id="left">
                <div class="article-wrap">
                    <h1>
                        <xsl:apply-templates select=".//tei:p[@rend='title-main']/node()"/>
                    </h1>
                    <xsl:for-each select=".//tei:p[starts-with(@rend,'title-') and @rend!='title-main']">
                        <p class="{@rend}">
                            <xsl:if test="@xml:lang"><xsl:attribute name="lang" select="@xml:lang"/></xsl:if>
                            <xsl:apply-templates/></p>
                    </xsl:for-each>
                    <xsl:apply-templates select=".//tei:body"/>
                </div>
            </div>
            <div id="drag-bar" class="drag-bar"></div>
            <div class="right-contents" id="right">
                <xsl:apply-templates select=".//tei:front"/>
            </div>
        </div>
    </xsl:template>


    <xsl:template match="tei:body">
        <section>
            <xsl:attribute name="class">
                <xsl:value-of select="local-name(.)"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
            <xsl:apply-templates select="following-sibling::tei:back"/>
        </section>
    </xsl:template>
    
    <xsl:template match="tei:back">
        <xsl:apply-templates select="child::tei:div[@type='bibliography']|child::tei:div[@type='appendix']"/>
    </xsl:template>

    <xsl:template match="tei:front">
        <section class="front">

            <div class="button-tabs">
                <xsl:if test="//tei:div[starts-with(@type,'section')]">
                    <button id="tabtn-toc" class="tab-btn focus-tab trans" data-key='toc' onclick="openTab('toc')"></button>
                </xsl:if>
                <xsl:if test="//tei:figure[not(child::tei:table)]">
                    <button id="tabtn-figures" class="tab-btn" onclick="openTab('figures')">Figures</button>
                </xsl:if>
                <xsl:if test="//tei:note">
                    <button id="tabtn-footnotes" class="tab-btn" onclick="openTab('footnotes')">Notes</button>
                </xsl:if>
                 <xsl:if test="//tei:div[@type='bibliography']">
                    <button id="tabtn-refs" class="tab-btn trans"  data-key='refs' onclick="openTab('refs')"></button>
                </xsl:if>
                <xsl:if test="//tei:div[@type='index']">
                    <button id="tabtn-index" class="tab-btn" onclick="openTab('index')">Index</button>
                </xsl:if>
                <button id="tabtn-metas" class="tab-btn" onclick="openTab('metas')">i</button>
            </div>

            <div id="toc" class="content-tab">
                <div id="toc-contents">

                    <ol>
                        <xsl:for-each select="../tei:body//tei:div|../tei:back//tei:div[@type='bibliography']|../tei:back//tei:listBibl|../tei:back//tei:div[@type='appendix']">
                            <xsl:call-template name="sec-toc-item">
                                <xsl:with-param name="item" select="."/>
                            </xsl:call-template>
                        </xsl:for-each>
                    </ol>

                </div>
            </div>
            
            <div id="figures" class="content-tab" style="display:none">
                <xsl:for-each select="..//tei:figure[not(child::tei:table) and not(child::tei:formula) and not(ancestor::tei:figure)]">
                    <xsl:apply-templates select="." mode="panel"/>
                </xsl:for-each>
            </div>
            
            <div id="footnotes" class="content-tab" style="display:none">
                <ul>
                    <xsl:for-each select="..//tei:note">
                        <xsl:apply-templates select="." mode="noteContent"/>
                    </xsl:for-each>
                </ul>
            </div>

            <div id="refs" class="content-tab" style="display:none">
                <ul>
                    <xsl:for-each select="../tei:back//tei:listBibl">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </ul>
            </div>
            
            <div id="index" class="content-tab" style="display:none">
                <xsl:for-each select="..//tei:div[@type='index']">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>    
            </div>

            <div id="metas" class="content-tab" style="display:none">
                <div>
                    <xsl:apply-templates select="preceding::tei:teiHeader"/>
<!--
                    <xsl:apply-templates select="./journal-meta"/>
                    <xsl:apply-templates select="./article-meta"/>
-->
                </div>
            </div>


        </section>
    </xsl:template>

    <!--footnote-->
    <xsl:template match="tei:note">
        <span class="xref-call fn-call">
            <xsl:attribute name="data-target">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:value-of select="@n"/>
        </span>
    </xsl:template>
    
    <xsl:template match="tei:note" mode="noteContent">
        <li class="footnote">
            <xsl:attribute name="id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <span class="label" title="voir"><xsl:value-of select="@n"/></span>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    
    <xsl:template match="tei:note/tei:p">
        <span>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!-- link -->
    <xsl:template match="tei:ref[@target]">
        <xsl:choose>
            <xsl:when test="@type='index'">
                <span class="index-link">
                    <a href="{@target}"><xsl:number/></a>
                </span>
             </xsl:when>
            <xsl:otherwise>
                <a target="_blank">
                    <xsl:apply-templates/>
                </a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:anchor"><span id="{@xml:id}">
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="starts-with(@n,'index')">index-anchor</xsl:when>
                    <xsl:otherwise>anchor</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:text>#</xsl:text>
        </span>
    </xsl:template>

    <!-- bibr -->
    <xsl:template match="xref[@ref-type='bibr']">
        <span class="xref-call bibr-call">
            <xsl:attribute name="data-target">
                <xsl:value-of select="@rid"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!--references-->
    <xsl:template match="ref[mixed-citation]">
        <div class="ref">
            <xsl:attribute name="id">
                <xsl:value-of select="@id"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="tei:listBibl">
        <div class="listBibl">
            <ul>
                <xsl:apply-templates/>
            </ul>
        </div>
    </xsl:template>
    
    <xsl:template match="tei:bibl[ancestor::tei:back]">
        <li class="bibl">
            <xsl:apply-templates/>
        </li>
    </xsl:template>


    <!-- sec toc -->
    <xsl:template name="sec-toc-item">
        <xsl:param name="item"/>
        <li>
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="local-name()='listBibl'">
                        <xsl:text>section2</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@type"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <a role="button">
                <xsl:attribute name="href">
                    <xsl:value-of select="concat('#title-', count(preceding::tei:head))"/>
                </xsl:attribute>
                <xsl:apply-templates select="$item/tei:head/node()"/>
                <xsl:if test="count($item/sec)>=1">
                    <ol>
                        <xsl:for-each select="$item/sec">
                            <xsl:call-template name="sec-toc-item">
                                <xsl:with-param name="item" select="."/>
                            </xsl:call-template>
                        </xsl:for-each>
                    </ol>
                </xsl:if>
            </a>
        </li>

    </xsl:template>

    <!--generic meta template (block) <=> teiHeader -->
    <xsl:template match="tei:teiHeader|tei:fileDesc|tei:titleStmt|tei:publicationStmt|tei:persName|tei:surname|tei:ab|tei:bibl|tei:publisher|tei:availability|tei:licence|tei:distributor|tei:date|tei:dim">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:author|tei:editor| journal-id | journal-title-group | publisher | title-group | article-title
        | publisher-name | article-categories | subj-group | subject | subtitle | trans-title-group | trans-title
        | abstract | trans-abstract | pub-date | kwd-group">
        <div>
            <xsl:attribute name="class">
                <xsl:value-of select="concat(local-name(.),' meta-field')"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
            <xsl:if test="child::tei:ref[@type='biography']">
                <xsl:variable name="bioLink" select="substring-after(child::tei:ref[@type='biography']/@target,'#')"/>
                <div class="bio">
                    <xsl:apply-templates select="ancestor::tei:TEI//tei:div[@type='biography' and @xml:id=$bioLink]/*"/>
                </div>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="tei:forename">
        <xsl:apply-templates/><xsl:text> </xsl:text>
    </xsl:template>
    
    <xsl:template match="tei:ab[@type='book']">
        <div class="ab-book-meta-field">
<!--            <node><xsl:copy-of select="current()"/></node>-->
            <xsl:if test="descendant::tei:dim[@type='pagination']/text() != ''">
                <xsl:text>Pagination : </xsl:text>
                <xsl:value-of select="descendant::tei:dim[@type='pagination']"/>
            </xsl:if>
        </div>
    </xsl:template>
    
    <!-- ignored teiHeader elements -->
    <xsl:template match="tei:sourceDesc|tei:encodingDesc|tei:profileDesc|tei:revisionDesc|tei:ab[@type='digital_download']"/>


    <!--generic inline template -->
    <xsl:template match="kwd | underline">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="local-name(.)"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    <xsl:template match="tei:hi">
        <xsl:choose>
            <xsl:when test="@rend='italic'">
                <i>
                    <xsl:apply-templates/>
                </i>
            </xsl:when>
            <xsl:when test="@rend='bold'">
                <b>
                    <xsl:apply-templates/>
                </b>
            </xsl:when>
            <xsl:when test="@rend='small-caps'">
                <span style="font-variant:small-caps;">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <xsl:when test="@rend='sup'">
                <sup>
                    <xsl:apply-templates/>
                </sup>
            </xsl:when>
            <xsl:when test="@rend='sub'">
                <sub>
                    <xsl:apply-templates/>
                </sub>
            </xsl:when>
            <xsl:when test="@rend='underline'">
                <span style="text-decoration:underline;">
                    <xsl:apply-templates/>
                </span>
            </xsl:when>
            <xsl:when test="@rend='strikethrough'">
                <s>
                    <xsl:apply-templates/>
                </s>
            </xsl:when>
            <xsl:when test="@rend='bold italic'">
                <b><i><xsl:apply-templates/></i></b>
            </xsl:when>
            <xsl:when test="@rend='small-caps italic'">
                <i><span style="font-variant:small-caps;"><xsl:apply-templates/></span></i>
            </xsl:when>
            <xsl:when test="@rend='sup italic'">
                <sup><i><xsl:apply-templates/></i></sup>
            </xsl:when>
            <xsl:when test="@rend='sub italic'">
                <sub><i><xsl:apply-templates/></i></sub>
            </xsl:when>
        </xsl:choose>        
    </xsl:template>

    <xsl:template match="styled-content">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="@style-type"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:index">
<!--        <span class="term"><xsl:apply-templates/></span>-->
    </xsl:template>
    
    <xsl:template match="tei:label[ancestor::tei:div[@type='index']]">
        <xsl:apply-templates/>
        <xsl:if test="parent::tei:item[parent::tei:list[@type='level1']]">
            <span class="toggle-button"> [+]</span>
        </xsl:if>
    </xsl:template>

    <xsl:template match="article-id">
        <div>
            <xsl:attribute name="class">
                <xsl:value-of select="@pub-id-type"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="contrib-group">
        <div class="contrib-group meta-field">
            <xsl:for-each select="./contrib">
                <div class="contrib">
                    <span class="surname">
                        <xsl:value-of select="./name/surname"></xsl:value-of>
                    </span>
                    <xsl:text> </xsl:text>
                    <span class="given-names">
                        <xsl:value-of select="./name/given-names"></xsl:value-of>
                    </span>
                    <xsl:for-each select="./xref">
                        <span>
                            <xsl:attribute name="class">
                                <xsl:value-of select="concat(@ref-type,' xref')"></xsl:value-of>
                            </xsl:attribute>
                            <xsl:value-of select="."></xsl:value-of>
                        </span>
                    </xsl:for-each>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>


    <xsl:template match="tei:div">
        <section>
            <xsl:if test="@type">
                <xsl:attribute name="class">
                    <xsl:value-of select="@type"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:attribute name="id">
                <xsl:choose>
                    <xsl:when test="@xml:id">
                        <xsl:value-of select="@xml:id"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="child::tei:head"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </section>
    </xsl:template>
    
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    <xsl:template match="tei:lb"><br/></xsl:template>

    <xsl:template match="tei:figure">
        <figure id="{concat('main-',@xml:id)}">
            <xsl:attribute name="class">
                <xsl:choose>
                    <xsl:when test="child::tei:table">fig-table</xsl:when>
                    <xsl:when test="child::tei:formula">fig-formula <xsl:value-of select="@rend"/></xsl:when>
                    <xsl:otherwise>fig-ill</xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </figure>
    </xsl:template>
    
    <xsl:template match="tei:figure" mode="panel">
        <figure id="{@xml:id}" class="figure">
            <xsl:apply-templates mode="panel"/>
        </figure>
    </xsl:template>

    <xsl:template match="tei:figure//tei:head" mode="panel">
        <label>
            <xsl:apply-templates/>
        </label>
    </xsl:template>
    
    <xsl:template match="tei:figure[not(child::tei:table)]//tei:head">
        <label>
            <span class="fig-call" data-target="{parent::tei:figure/@xml:id}"><xsl:apply-templates/></span>
            <span class="fig-call-window">
            <a href="#" id="openFigureLink">
                <xsl:attribute name="onclick">
                    <xsl:value-of select='concat("openFigureWindow&#40;&apos;",parent::tei:figure/@xml:id,"&apos;&#41;")'/>
                </xsl:attribute>
                <xsl:text>     ↗ </xsl:text>
                </a>
            </span>
        </label>
    </xsl:template>

    <xsl:template match="tei:graphic" mode="panel">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="substring-after(@url,'icono/br/')"/>
            </xsl:attribute>
        </img>
    </xsl:template>
    
 
    <xsl:template match="tei:graphic[parent::tei:figure[not(child::tei:head)]]">
        <label class="generated">
            <span class="fig-call" data-target="{parent::tei:figure/@xml:id}">[Ill.]</span>
            <span class="fig-call-window">
            <a href="#" id="openFigureLink">
                <xsl:attribute name="onclick">
                    <xsl:value-of select='concat("openFigureWindow&#40;&apos;",parent::tei:figure/@xml:id,"&apos;&#41;")'/>
                </xsl:attribute>
                <xsl:text>     ↗ </xsl:text>
                </a>
            </span>
        </label>
    </xsl:template>
 
    
    <xsl:template match="tei:p[@rend='caption' or @rend='credits']">
        <p class="{@rend}">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    
    <!-- table -->
    <xsl:template match="tei:figure[child::tei:table]//tei:head"/>
    
    <xsl:template match="tei:figure[child::tei:table]//tei:head" mode="titleUp">
        <xsl:apply-templates/>
    </xsl:template>
        
    <xsl:template match="tei:table">
        <xsl:if test="parent::tei:figure/tei:head">
            <span class="expand"><xsl:text>▸ </xsl:text></span>
            <label><!--span class="table-call"><xsl:text> ↓ </xsl:text></span--><xs:apply-templates select="parent::tei:figure/tei:head" mode="titleUp"/><!--span class="table-call"><xsl:text> ↓ </xsl:text></span--></label>
            <span class="table-call-window"><a href="#" id="openFigureLink">
                <xsl:attribute name="onclick"><xsl:value-of select='concat("openFigureWindow&#40;&apos;main-",parent::tei:figure/@xml:id,"&apos;&#41;")'/></xsl:attribute>
                <xsl:text> ↗ </xsl:text></a></span>
        </xsl:if>
        <xsl:if test="parent::tei:figure[not(child::tei:head)]">
            <span class="expand empty"><xsl:text>▸ </xsl:text>
            </span>
            <label class="generated"/>
            <span class="table-call-window"><a href="#" id="openFigureLink">
                <xsl:attribute name="onclick"><xsl:value-of select='concat("openFigureWindow&#40;&apos;main-",parent::tei:figure/@xml:id,"&apos;&#41;")'/></xsl:attribute>
                <xsl:text> ↗ </xsl:text></a></span>
        </xsl:if>
        <table>
            <xsl:choose>
                <xsl:when test="label or title">                                
                    <caption>
                        <xsl:value-of select="label"/>
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select=".//tei:head"/>
                    </caption>
                    <xsl:apply-templates/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </table>
    </xsl:template>

    <xsl:template match="tei:row">
        <tr><xsl:apply-templates/></tr>
    </xsl:template>
    
    <xsl:template match="tei:cell">
        <td>
            <xsl:if test="@cols"><xsl:attribute name="colspan" select="@cols"/></xsl:if>
            <xsl:if test="@rows"><xsl:attribute name="rowspan" select="@rows"/></xsl:if>
            <xsl:apply-templates/>
        </td>
    </xsl:template>

    <!-- head -->
    <xsl:template match="tei:head[not(parent::tei:figure)]">
        <xsl:variable name="ancestors">
            <xs:value-of select="count(ancestor::tei:div) + count(ancestor::tei:listBibl)"/>
            <!--  + count(ancestor::caption) -->
        </xsl:variable>

        <xsl:variable name="n">
            <xs:value-of select="count(preceding::tei:head)"/>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="parent::tei:div[@type='index'] and text()='Index'"/>
            <xsl:otherwise>
                <xsl:element name="h{$ancestors + 1}">
                    <xsl:attribute name="id"><xsl-:value-of select="concat('title-',$n)"/></xsl:attribute>
                    <xsl:apply-templates/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- boxed-text -->
    <xsl:template match="tei:floatingText">
        <div class="boxed-text">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    
    <!-- liste -->
    <xsl:template match="tei:list">
        <xsl:variable name="listType">
            <xsl:choose>
                <xsl:when test="@type='ordered'">ol</xsl:when>
                <xsl:otherwise>ul</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="attributeValue">
            <xsl:value-of select="concat('list-style-type:',substring-after(@rendition,'#list-'))"/>
        </xsl:variable>
        <xsl:element name="{$listType}">
            <xsl:attribute name="class">
                <xsl:text>index-</xsl:text><xsl:value-of select="@type"/>
            </xsl:attribute> 
<!--            select="concat('index-',@type)"/>-->
            <xsl:attribute name="id" select="@xml:id"/>
            <xsl:attribute name="style">
                <xsl:choose>
                    <xsl:when test="contains($attributeValue,'ndash')">
                        <xsl:text>list-style-type: "– ";</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$attributeValue"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    
    <!-- citations -->
    <xsl:template match="tei:cit[not(@type='linguistic')]">
        <blockquote><xsl:apply-templates/></blockquote>
    </xsl:template>
    
    <xsl:template match="tei:quote">
        <p><xsl:apply-templates/></p>
    </xsl:template>
    
    <!-- maths -->
    <xsl:template match="tei:formula[@notation='mathml']|tei:formula[@notation='mml']">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:formula[@notation='latex']">
        <xsl:choose>
            <xsl:when test="parent::tei:figure[@rend='inline']">
                <span class="{@notation}">
                    <xsl:copy-of select="concat('$$',node(),'$$')"/>
                </span>
            </xsl:when>
            <xsl:otherwise>
                <div class="formula-tex">
                    <xsl:copy-of select="@*"/>
                    <xsl:copy-of select="concat('$$',node(),'$$')"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="*[namespace-uri()='http://www.w3.org/1998/Math/MathML']">
        <xsl:element name="{local-name()}">
            <xsl:for-each select="@*">
              <xsl:if test="name() != 'display'">
                <xsl:attribute name="{name()}">
                  <xsl:value-of select="."/>
                </xsl:attribute>
              </xsl:if>
            </xsl:for-each>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>
    
    <!-- linguistique -->
    <xsl:template match="tei:cit[@type='linguistic']">
        <table class="linguistic">
            <tbody>
                <xsl:for-each select="child::tei:quote/*">
                    <tr>
                        <xsl:apply-templates select="."/>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>

    <xsl:template match="tei:label">
        <td>
            <xsl:apply-templates/>
        </td>
    </xsl:template>

    <xsl:template match="tei:seg">
        <xsl:choose>
            <xsl:when test="not(preceding-sibling::tei:seg or following-sibling::tei:seg) and parent::tei:quote[@type='example']">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                <td>
                    <xsl:apply-templates/>
                </td>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:num[not(parent::tei:label)]">
        <td>
            <xsl:apply-templates/>
        </td>
    </xsl:template>

    <xsl:template match="tei:quote[ancestor::tei:cit[@type='linguistic']]">
        <xsl:variable name="maxCount">
          <xsl:for-each select="ancestor::tei:cit[@type='linguistic']/tei:quote/tei:quote[@type='example']">
            <xsl:sort select="count(*)" data-type="number" order="descending"/>
            <xsl:if test="position() = 1">
              <xsl:value-of select="count(*)"/>
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="nbSeg">
            <xsl:value-of select="count(child::tei:seg)"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="@type='trl'">
                <td>
                    <xsl:attribute name="number-columns-spanned">
                        <xsl:value-of select="$maxCount"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </td>
            </xsl:when>
            <xsl:when test="$nbSeg='1'">
                <td>
                    <xsl:attribute name="number-columns-spanned">
                        <xsl:value-of select="$maxCount"/>
                    </xsl:attribute>
                        <xsl:apply-templates/>
                </td>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    

</xsl:stylesheet>
