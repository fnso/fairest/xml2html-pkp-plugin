const dict = {
    fr : {
        'toc' : 'Table des matières',
        'refs': 'Références',
    },
    en: {
        'toc' : 'Table of contents',
        'refs': 'References',
    }
}

function getText(key){
    return dict[lang][key];
}

function translatePage(newLang){
    lang = newLang;
    document.querySelectorAll('.trans').forEach((elt) => {
        let key = elt.dataset.key;
        elt.innerHTML = getText(key);
    })
}


window.addEventListener('DOMContentLoaded', (event) => {
    lang = lang.split('_')[0];
    translatePage(lang);

    // Appel fonction traitant ensemble notes et figures : 
    bindFootnotesAndImages();

    // Appel fonction pour tableaux déroulants 
    initialiseTableaux();
    
    // index collapsible
    initializeCollapsibleLists();
    
    // open new windows
    openFigureWindow(elementId); 

    //toc hightlighing on scroll
    document.querySelector('.left-contents').addEventListener("scroll", () => {
        let scrollTop = document.querySelector('.left-contents').scrollTop;
        let anchors = document.querySelector('.article-wrap').querySelectorAll('h2, h3, h4');
        for (let i = 0; i < anchors.length; i++){
            let tocItemRef = '#' + anchors[i].getAttribute('id');
            let tocItem = document.getElementById("toc-contents").querySelector("a[href='"+tocItemRef+"']");
            if(!tocItem)
                continue;
            //console.log(anchors[i].offsetTop, anchors[i].clientHeight)
            if (scrollTop + 100> anchors[i].offsetTop) {
                document.getElementById("toc-contents").querySelectorAll('a').forEach(
                    (a) => a.classList.remove('active')
                )
                tocItem.classList.add('active');
            }
        }
    });
});


function openTab(tabName) {
    let i;
    let tabs = document.querySelectorAll(".content-tab");
    for (i = 0; i < tabs.length; i++) {
        tabs[i].style.display = "none";
    }
    document.getElementById(tabName).style.display = "block";

    //button focus
    document.querySelectorAll(".tab-btn").forEach((btn) => {
        btn.classList.remove('focus-tab');
    });
    document.getElementById('tabtn-' + tabName).classList.add('focus-tab');
}


function darkMode(bool) {
    if (bool) {
        document.getElementById('moon').style.display = "block";
        document.getElementById('sun').style.display = "none";
    } else {
        document.getElementById('sun').style.display = "block";
        document.getElementById('moon').style.display = "none";
    }
    document.body.classList.toggle('dark');
}

/////////////////////////////////////////////////////////////////
//////    Fonction pour figures et notes ensemble  [XG]   ///////
/////////////////////////////////////////////////////////////////

function bindFootnotesAndImages() {
    let figAppels = document.getElementsByClassName('fig-call');
    let notesAppels = document.getElementsByClassName('xref-call');
    // fusionne les deux tableaux ('arrays') dans un 3e : 
    let tousLesAppels = [...figAppels, ...notesAppels];

    for (let i=0 ; i < tousLesAppels.length ; i++) {
        let focusTarget = document.getElementById(tousLesAppels[i].dataset.target);
        tousLesAppels[i].addEventListener('click', () => {
            if(tousLesAppels[i].classList.contains('fn-call')) {
                openTab('footnotes');
            } else if (tousLesAppels[i].classList.contains('fig-call')){
                let cible = tousLesAppels[i].getAttribute('data-target');
                openTab('figures');
                scrollToElementById(cible);
            } else {
                openTab('refs');
            }

            document.getElementById(tousLesAppels[i].dataset.target).scrollIntoView({
                block: 'center',
                behavior: "smooth"
            });
            document.querySelectorAll('.footnote, .ref').forEach(
                (fn) => fn.classList.remove('focus')
            );
            focusTarget.classList.add('focus');

        });

        if(tousLesAppels[i].classList.contains('fn-call')) {
            let footnoteLabel = focusTarget.querySelector('span.label');
            footnoteLabel.addEventListener('click', () => {
                tousLesAppels[i].scrollIntoView({
                    block: 'start',
                    behavior: "smooth"
                });
                document.querySelectorAll('.footnote').forEach(
                    (fn) => fn.classList.remove('focus')
                );
                focusTarget.classList.add('focus');
            })
        }

    }
}

function scrollToElementById(elementId) {
    const targetElement = document.getElementById(elementId);
    if (targetElement) {
        targetElement.scrollIntoView({ behavior: 'smooth' });
    }
}


////////////////////////////////////////////////
//////    Fonctions pour les tables   [XG]   ///
////////////////////////////////////////////////

// [EC 4/9/23] table collapsible – v. draft - version XG - même effet, approche différente.

	let figTables;

	function initialiseTableaux() {

	figTables = document.getElementsByClassName("fig-table");

	for (let i = 0; i<figTables.length; i++ ) {

    let labels = figTables[i].getElementsByTagName("label");
    let expand = figTables[i].getElementsByClassName("expand");    

    labels[0].setAttribute('onclick', "afficheTableau('"+i+"');");
    expand[0].setAttribute('onclick', "afficheTableau('"+i+"');");
    }    
}


function afficheTableau(numero) {
    let tableCourante = figTables[numero].getElementsByTagName("table")[0];
    let label = figTables[numero].getElementsByTagName("label")[0];
    let triangle = figTables[numero].getElementsByClassName("expand")[0]; 
    
    if (tableCourante.style.display === 'none' || tableCourante.style.display === '') {
        tableCourante.style.display = 'table'; 
        tableCourante.style.backgroundColor = 'white';
        triangle.textContent = "▾";
    } else {
        tableCourante.style.display = 'none';
        triangle.textContent = "▸"; 
    }
}


/////////////////////////////////////////////////////////////
//////    Fonctions pour les index (collapsible)   [EC]   ///
/////////////////////////////////////////////////////////////
function initializeCollapsibleLists() {
  var toggleButtons = document.querySelectorAll('.toggle-button');
  toggleButtons.forEach(function (button) {
    button.addEventListener('click', function () {
      var parentLi = this.closest('li'); // Get the parent <li> element
      var indexLevel2Elements = parentLi.querySelectorAll('.index-level2'); // Get descendant elements with class .index-level2
      indexLevel2Elements.forEach(function (element) {
        element.style.display = (element.style.display === 'none' || element.style.display === '') ? 'block' : 'none'; // Toggle display
      });

      // Toggle button text on the clicked toggle button
      this.textContent = (indexLevel2Elements[0].style.display === 'none') ? '[+]' : '[-]'; // Use the first element's display property
    });
  });
}


///////////////////////////////////////////////////////
//////    Fonctions pour fenêtres externes   [EC]   ///
///////////////////////////////////////////////////////
function openFigureWindow(elementId) {
  // Create a new window with specific dimensions and position
  const newWindow = window.open('', '_blank', 'width=900,height=600');

  // Check if the window was opened successfully
  if (newWindow) {
    // Access the document of the new window
    const newWindowDocument = newWindow.document;

    // Create a <style> element for custom CSS
    const styleElement = newWindowDocument.createElement('style');
    
    // Define your custom CSS styles
    const customStyles = `
      /* Add your CSS styles here */
      * {font-family: "Noto Sans",-apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Oxygen-Sans","Ubuntu","Cantarell","Helvetica Neue",sans-serif;}
      figure {
            width: 100%;
            min-height: 100px;
            display:flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            color: #999;
            margin: 25px 0px;
        }
      label {background-color: #454545;width:90%;}
      span.expand {display:none;}
      table, img {
            max-width:90%
        }
      td,th {
            border-bottom:1px solid #ccc;
            font-size:1rem;
        }

        .fig-table label {
            padding: 10px;
            margin: 0;
            cursor: pointer;
        }

        .credits {
            margin: 0 5 5 20px;
            font-size:smaller;
            align-self: flex-start;
        }

        .table-call-window,
        .fig-call-window {
            display:none;
        }
    `;
    
    // Set the CSS content of the <style> element
    styleElement.textContent = customStyles;
    
    // Append the <style> element to the new window's document head
    newWindowDocument.head.appendChild(styleElement);

    // Find the element with the specified ID in the current document
    const elementToClone = document.getElementById(elementId);

    // Clone the element to the new window's document
    const clonedElement = elementToClone.cloneNode(true);

    // Ensure that the cloned table is always visible
    const tableInClone = clonedElement.querySelector('table');
    if (tableInClone) {
        // If a table exists, set its display style to 'table'
        tableInClone.style.display = 'table';
    }

    // Append the cloned element to the new window's document body
    newWindowDocument.body.appendChild(clonedElement);
  }
}

// Drag div
document.addEventListener('DOMContentLoaded', function() {
    const dragBar = document.getElementById('drag-bar');
    const leftPane = document.getElementById('left');
    const rightPane = document.getElementById('right');
    let isDragging = false;
    let startX;
    let initialWidthLeft;
    let initialWidthRight;

    dragBar.addEventListener('mousedown', (e) => {
        isDragging = true;
        startX = e.clientX;
        initialWidthLeft = leftPane.offsetWidth;
        initialWidthRight = rightPane.offsetWidth;

        document.addEventListener('mousemove', onMouseMove);
        document.addEventListener('mouseup', onMouseUp);
    });

    function onMouseMove(e) {
        if (!isDragging) return;

        const newWidthLeft = initialWidthLeft + e.clientX - startX;
        const newWidthRight = initialWidthRight - e.clientX + startX;

        if (newWidthLeft > 100 && newWidthRight > 100) {
            leftPane.style.width = `${newWidthLeft}px`;
            rightPane.style.width = `${newWidthRight}px`;
        }
    }

    function onMouseUp() {
        isDragging = false;
        document.removeEventListener('mousemove', onMouseMove);
        document.removeEventListener('mouseup', onMouseUp);
    }
});