<?php
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;
use PKP\config\Config;
use PKP\file\FileManager;
use PKP\plugins\Hook;
use APP\handler\Handler;
class ImageUtils
{
    public static function buildFileSystem(Handler $handler) : Filesystem{
        $umask = Config::getVar('files', 'umask', 0022);
        $adapter = new LocalFilesystemAdapter(
            Config::getVar('files', 'files_dir'),
            PortableVisibilityConverter::fromArray([
                'file' => [
                    'public' => FileManager::FILE_MODE_MASK & ~$umask,
                    'private' => FileManager::FILE_MODE_MASK & ~$umask,
                ],
                'dir' => [
                    'public' => FileManager::DIRECTORY_MODE_MASK & ~$umask,
                    'private' => FileManager::DIRECTORY_MODE_MASK & ~$umask,
                ]
            ]),
            LOCK_EX,
            LocalFilesystemAdapter::DISALLOW_LINKS
        );

        Hook::call('File::adapter', [&$adapter, $handler]);

        return new Filesystem($adapter);
    }

    public static function downloadImage(Filesystem $fs, string $path, string $name){
        $mimetype = 'application/octet-stream';
        $filesize = $fs->fileSize($path);
        $encodedFilename = urlencode($name);
        header("Content-Type: {$mimetype}");
        header("Content-Length: {$filesize}");
        header('Accept-Ranges: none');
        header('Content-Disposition: inline' . ";filename=\"{$encodedFilename}\";filename*=UTF-8''{$encodedFilename}");
        header('Cache-Control: private'); // Workarounds for IE weirdness
        header('Pragma: public');
        fpassthru($fs->readStream($path));
        exit;
    }
}