<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:mml="http://www.w3.org/1998/Math/MathML"
                xmlns:xs="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsl-="http://www.w3.org/1999/XSL/Transform"
                exclude-result-prefixes="xlink mml">

    <xsl:output method="html" omit-xml-declaration="yes"/>

    <xsl:strip-space elements="*"/>

    <xsl:param name="lang"/>

    <!-- Space is preserved in all elements allowing #PCDATA -->
    <!--<xsl:preserve-space-->
            <!--elements="abbrev abbrev-journal-title access-date addr-line-->
              <!--aff alt-text alt-title article-id article-title-->
              <!--attrib award-id bold chapter-title chem-struct-->
              <!--collab comment compound-kwd-part compound-subject-part-->
              <!--conf-acronym conf-date conf-loc conf-name conf-num-->
              <!--conf-sponsor conf-theme contrib-id copyright-holder-->
              <!--copyright-statement copyright-year corresp country-->
              <!--date-in-citation day def-head degrees disp-formula-->
              <!--edition elocation-id email etal ext-link fax fpage-->
              <!--funding-source funding-statement given-names glyph-data-->
              <!--gov inline-formula inline-supplementary-material-->
              <!--institution isbn issn-l issn issue issue-id issue-part-->
              <!--issue-sponsor issue-title italic journal-id-->
              <!--journal-subtitle journal-title kwd label license-p-->
              <!--long-desc lpage meta-name meta-value mixed-citation-->
              <!--monospace month named-content object-id on-behalf-of-->
              <!--overline p page-range part-title patent person-group-->
              <!--phone prefix preformat price principal-award-recipient-->
              <!--principal-investigator product pub-id publisher-loc-->
              <!--publisher-name related-article related-object role-->
              <!--roman sans-serif sc season self-uri series series-text-->
              <!--series-title sig sig-block size source speaker std-->
              <!--strike string-name styled-content std-organization-->
              <!--sub subject subtitle suffix sup supplement surname-->
              <!--target td term term-head tex-math textual-form th-->
              <!--time-stamp title trans-source trans-subtitle trans-title-->
              <!--underline uri verse-line volume volume-id volume-series-->
              <!--xref year-->

              <!--mml:annotation mml:ci mml:cn mml:csymbol mml:mi mml:mn-->
              <!--mml:mo mml:ms mml:mtext"/>-->


    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>


    <xsl:template match="/">
        <div class="wrap">
            <a role="button" onclick="translatePage('fr')">FR</a> | <a role="button" onclick="translatePage('en')">EN</a>
            <div id="mode-color-btns">
                <button id="sun" onclick="darkMode(true)">
                    <svg style="" class="icon" focusable="false" viewBox="0 0 32 32">
                        <path d="M16 12.005a4 4 0 1 1-4 4a4.005 4.005 0 0 1 4-4m0-2a6 6 0 1 0 6 6a6 6 0 0 0-6-6z"
                              fill="#adbac7"></path>
                        <path d="M5.394 6.813l1.414-1.415l3.506 3.506L8.9 10.318z" fill="#adbac7"></path>
                        <path d="M2 15.005h5v2H2z" fill="#adbac7"></path>
                        <path d="M5.394 25.197L8.9 21.691l1.414 1.415l-3.506 3.505z" fill="#adbac7"></path>
                        <path d="M15 25.005h2v5h-2z" fill="#adbac7"></path>
                        <path d="M21.687 23.106l1.414-1.415l3.506 3.506l-1.414 1.414z" fill="#adbac7"></path>
                        <path d="M25 15.005h5v2h-5z" fill="#adbac7"></path>
                        <path d="M21.687 8.904l3.506-3.506l1.414 1.415l-3.506 3.505z" fill="#adbac7"></path>
                        <path d="M15 2.005h2v5h-2z" fill="#adbac7"></path>
                    </svg>
                </button>
                <button id="moon" onclick="darkMode(false)" style="display:none;">
                    <svg style="" class="icon" focusable="false" viewBox="0 0 32 32">
                        <path d="M13.502 5.414a15.075 15.075 0 0 0 11.594 18.194a11.113 11.113 0 0 1-7.975 3.39c-.138
                    0-.278.005-.418 0a11.094 11.094 0 0 1-3.2-21.584M14.98 3a1.002 1.002 0 0 0-.175.016a13.096 13.096
                    0 0 0 1.825 25.981c.164.006.328 0 .49 0a13.072 13.072 0 0 0 10.703-5.555a1.01 1.01 0 0
                    0-.783-1.565A13.08 13.08 0 0 1 15.89 4.38A1.015 1.015 0 0 0 14.98 3z" fill="#adbac7">
                        </path>
                    </svg>
                </button>
            </div>

            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="article">
        <div class="article-and-metas">
            <div class="left-contents" id="left">
                <div class="article-wrap">
                    <h1>
                        <xsl:value-of select="./front/article-meta//article-title"/>
                    </h1>
                    <xsl:apply-templates select="./body"/>
                </div>
            </div>
            <div id="drag-bar" class="drag-bar"></div>
            <div class="right-contents" id="right">
                <xsl:apply-templates select="./front"/>
            </div>
        </div>
        <xsl:apply-templates select="./back"/>
    </xsl:template>


    <xsl:template match="body | back">
        <section>
            <xsl:attribute name="class">
                <xsl:value-of select="local-name(.)"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
        </section>
    </xsl:template>

    <xsl:template match="front">
        <section class="front">
            <div class="button-tabs">
                <button id="tabtn-toc" class="tab-btn focus-tab trans" data-key='toc' onclick="openTab('toc')"></button>
                <button id="tabtn-refs" class="tab-btn trans" data-key='refs' onclick="openTab('refs')">
                </button>
                <button id="tabtn-footnotes" class="tab-btn" onclick="openTab('footnotes')">Notes</button>
                <button id="tabtn-metas" class="tab-btn" onclick="openTab('metas')">i</button>
            </div>

            <div id="toc" class="content-tab">
                <div id="toc-contents">

                    <ol>
                        <xsl:for-each select="../body/sec">
                            <xsl:call-template name="sec-toc-item">
                                <xsl:with-param name="item" select="."/>
                            </xsl:call-template>
                        </xsl:for-each>
                    </ol>

                </div>
            </div>

            <div id="refs" class="content-tab" style="display:none">
                <ul>
                    <xsl:for-each select="../back//ref">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </ul>
            </div>

            <div id="footnotes" class="content-tab" style="display:none">
                <ul>
                    <xsl:for-each select="../back//fn">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </ul>
            </div>

            <div id="metas" class="content-tab" style="display:none">
                <div>
                    <xsl:apply-templates select="./journal-meta"/>
                    <xsl:apply-templates select="./article-meta"/>
                </div>
            </div>


        </section>
    </xsl:template>

    <!--footnote-->
    <xsl:template match="xref[@ref-type='fn']">
        <span class="xref-call fn-call">
            <xsl:attribute name="data-target">
                <xsl:value-of select="@rid"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>


    <xsl:template match="fn">
        <li class="footnote">
            <xsl:attribute name="id">
                <xsl:value-of select="@id"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </li>
    </xsl:template>

    <xsl:template match="fn/label[text()]">
        <span class="label" title="voir">
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    <xsl:template match="fn/p">
        <span>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!--link-->
    <xsl:template match="ext-link[@xlink:href]">
        <a href="{@xlink:href}"><xsl:apply-templates/></a>
    </xsl:template>
    
    <!-- bibr -->
    <xsl:template match="xref[@ref-type='bibr']">
        <span class="xref-call bibr-call">
            <xsl:attribute name="data-target">
                <xsl:value-of select="@rid"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <!--references-->
    <xsl:template match="ref[mixed-citation]">
        <div class="ref">
            <xsl:attribute name="id">
                <xsl:value-of select="@id"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="mixed-citation">
        <div class="mixed-citation">
            <xsl:apply-templates/>
        </div>
    </xsl:template>


    <!-- sec toc -->
    <xsl:template name="sec-toc-item">
        <xsl:param name="item"/>
        <li>
            <a role="button">
                <xsl:attribute name="href">
                    <xsl:value-of select="concat('#title-', count(preceding::title))"/>
                </xsl:attribute>
                <xsl:value-of select="$item/title"/>
                <xsl:if test="count($item/sec)>=1">
                    <ol>
                        <xsl:for-each select="$item/sec">
                            <xsl:call-template name="sec-toc-item">
                                <xsl:with-param name="item" select="."/>
                            </xsl:call-template>
                        </xsl:for-each>
                    </ol>
                </xsl:if>
            </a>
        </li>

    </xsl:template>

    <!--generic meta template (block) -->
    <xsl:template match="journal-meta | journal-id | journal-title-group | publisher | title-group | article-title
        | publisher-name | article-categories | subj-group | subject | subtitle | trans-title-group | trans-title
        | abstract | trans-abstract | pub-date | kwd-group">
        <div>
            <xsl:attribute name="class">
                <xsl:value-of select="concat(local-name(.),' meta-field')"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>


    <!--generic inline template -->
    <xsl:template match="kwd | underline">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="local-name(.)"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>


    <xsl:template match="bold">
        <strong>
            <xsl:apply-templates/>
        </strong>
    </xsl:template>

    <xsl:template match="styled-content">
        <span>
            <xsl:attribute name="class">
                <xsl:value-of select="@style-type"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>


    <xsl:template match="article-id">
        <div>
            <xsl:attribute name="class">
                <xsl:value-of select="@pub-id-type"></xsl:value-of>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="contrib-group">
        <div class="contrib-group meta-field">
            <xsl:for-each select="./contrib">
                <div class="contrib">
                    <span class="surname">
                        <xsl:value-of select="./name/surname"></xsl:value-of>
                    </span>
                    <xsl:text> </xsl:text>
                    <span class="given-names">
                        <xsl:value-of select="./name/given-names"></xsl:value-of>
                    </span>
                    <xsl:for-each select="./xref">
                        <span>
                            <xsl:attribute name="class">
                                <xsl:value-of select="concat(@ref-type,' xref')"></xsl:value-of>
                            </xsl:attribute>
                            <xsl:value-of select="."></xsl:value-of>
                        </span>
                    </xsl:for-each>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>


    <xsl:template match="article-meta">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="journal-title">
        <h1>
            <xsl:apply-templates/>
        </h1>
    </xsl:template>

    <xsl:template match="sec">
        <section>
            <xsl:apply-templates/>
        </section>
    </xsl:template>

    <xsl:template match="fig">
        <figure>

            <xsl:apply-templates/>
        </figure>
    </xsl:template>

    <xsl:template match="fig//title">
        <caption>
            <xsl:apply-templates/>
        </caption>
    </xsl:template>

    <xsl:template match="graphic">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="@xlink:href"/>
            </xsl:attribute>
        </img>
    </xsl:template>


    <xsl:template match="title">
        <xsl:variable name="ancestors">
            <xs:value-of select="count(ancestor::sec) + count(ancestor::caption)"/>
        </xsl:variable>

        <xsl:variable name="n">
            <xs:value-of select="count(preceding::title)"/>
        </xsl:variable>


        <xsl:element name="h{$ancestors + 1}">
            <xsl:attribute name="id"><xsl-:value-of select="concat('title-',$n)"/></xsl:attribute>
            <xsl:apply-templates></xsl:apply-templates>
        </xsl:element>
    </xsl:template>


    <!-- boxed-text -->
    <xsl:template match="boxed-text">
        <div class="boxed-text">
            <xsl:apply-templates/>
        </div>
    </xsl:template>



    <!-- table -->
    <xsl:template match="table-wrap">
        <xsl:choose>
            <xsl:when test="label or title">
                <table>

                        <caption>
                            <xsl:value-of select="label"/>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select=".//title"/>
                        </caption>

                    <xsl:apply-templates></xsl:apply-templates>
                </table>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates></xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- list -->
    <xsl:template match="list[@list-type='ordered']">
        <ol>
            <xsl:apply-templates/>
        </ol>
    </xsl:template>

    <xsl:template match="list[@list-type='unordered' or not(@list-type)]">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>

    <xsl:template match="list-item">
        <li><xsl:apply-templates/></li>
    </xsl:template>



    <xsl:template match="italic">
        <em>
            <xsl:apply-templates/>
        </em>
    </xsl:template>


    <!-- mathml -->
    <xsl:template match="disp-formula[@content-type='math/mathml']">
        <div class="disp-formula">
            <xsl:apply-templates/>
        </div>
    </xsl:template>


    <xsl:template match="mml:*">
        <xsl:element name="{local-name(.)}">
            <xsl:apply-templates select="@*|*|text()"/>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
