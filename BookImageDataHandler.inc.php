<?php

use APP\core\Application;
use APP\facades\Repo;
use APP\pages\catalog\CatalogBookHandler;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;
use PKP\config\Config;
use PKP\file\FileManager;
use PKP\plugins\Hook;

class BookImageDataHandler extends CatalogBookHandler
{

    private $fs;

    public function __construct()
    {
        parent::__construct();
        $this->fs = ImageUtils::buildFileSystem($this);
    }

    /**
     * Image from zip archive submission file
     * @param array $args
     * @param Request $request
     * @return mixed
     */
    public function zimage(array $args, APP\core\Request $request)
    {
        $pubId= $args[1];
        $imgName = $args[2];
        $submission = $this->getAuthorizedContextObject(Application::ASSOC_TYPE_SUBMISSION);


        $pubFormatFiles = Repo::submissionFile()
            ->getCollector()
            ->filterBySubmissionIds([$submission->getId()])
            ->filterByAssoc(Application::ASSOC_TYPE_PUBLICATION_FORMAT)
            ->getMany();

        $publicationFile = NULL;
        foreach ($pubFormatFiles as $file){
            if($file->getData('assocId') == $pubId) {
                $publicationFile = $file;
            }
        }

        $pathInfo = pathinfo($publicationFile->getData('path'));
        $imagePath = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'] . DIRECTORY_SEPARATOR . $imgName;
        ImageUtils::downloadImage($this->fs, $imagePath, $imgName);

    }


}
