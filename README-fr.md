# XML To HTML (Métopes to PKP) PLUGIN



[toc]



Plugin OJS/OMP de chargement de flux XML développé dans le cadre du projet [FNSO FAIREST](https://www.ouvrirlascience.fr/fair-environment-for-structured-editing/) ([IR Métopes](https://www.metopes.fr/) et [Certic](https://www.certic.unicaen.fr/home/)) : 

- génère une page HTML pour la diffusion plein-texte ;
- via transformation XSLT ;
- à partir de flux XML-JATS Publishing 1.3 ou XML-TEI Commons Publishing ; 
- chargement des images associées via la même archive ;
- possibilités de personnalisation des éléments de mise en forme (template, CSS, javascript).



**Licence** : CeCILL-B (voir [licence](LICENCE)).

**Exemple** : https://puc-editorial.unicaen.fr/ojs-3.4/index.php/Caen/article/view/2/14



**Ce plugin est en cours de développement, son usage n'est pas destiné à être utilisé en production.**



## Installation

### Prérequis

Pré-requis : [modules php recommandés par OJS](https://docs.pkp.sfu.ca/admin-guide/3.3/en/requirements) + `php-zip`.



### Installation

````
cd ojs-3.3.0-7/plugins/generic
git clone git@git.unicaen.fr:fnso/fairest/xml2html-pkp-plugin.git xml2html
````


> N.B. : ne pas insérer de tiret dans le nom de dossier du plugin.



### Activation

Activer le plugin depuis la section *Site web > Modules externes* (requiert un rôle avec niveau d'autorisation suffisant).



### Paramètres

Depuis *Site web* > *Modules externes* > *Modules installés* : XML To HTML PLUGIN > *Paramètres* : choisir le *Format / Vocabulaire XML* dans le menu déroulant : JATS ou TEI.



Choix de la XSL d'import à mobiliser : TEI ou JATS.

Deux transformations sont fournies dans le plugin : 

 - intégration d'un flux XML-JATS (`jats2html.xsl`, fork de https://github.com/ncbi/JATSPreviewStylesheets/blob/master/xslt/main/jats-html.xsl) ;
 - intégration d'un flux XML-TEI *Commons Publishing* (`tei2html.xsl`).



### Compatibilité

Plugin testé sur les versions suivantes : 

- OJS 3.4 (3.4.0-4)
- OMP 3.4 (3.4.0-4)



### Personnalisation

- Template de page : `HTMLGalleyView.tpl` ([lien](https://git.unicaen.fr/fnso/fairest/xml2html-pkp-plugin/-/blob/master/templates/HTMLGalleyView.tpl?ref_type=heads))
- Javascript : `resources/javascript/xml_2html.js` ([lien](https://git.unicaen.fr/fnso/fairest/xml2html-pkp-plugin/-/blob/master/resources/javascript/xml_2_html.js?ref_type=heads))
- CSS : `resources/styles/xml_2html.css` ([lien](https://git.unicaen.fr/fnso/fairest/xml2html-pkp-plugin/-/blob/master/resources/styles/xml_2_html.css?ref_type=heads))



## Fonctionnement utilisateur
### OMP : procédure de chargement des fichiers

Ces étapes sont réalisées à l'étape *Publication* > onglet *Formats de publication*.



#### Définir le format de publication HTML

Lors de la <u>première création</u> d'une publication au format HTML pour un ouvrage :

- Ajouter un format de publication
  - Nom : "HTML"

  - Type : *Digital (DA)*



#### Ajouter les fichiers sources pour la production des pages HTML

Ajouter les fichiers qui génèreront les pages HTML des chapitres : 

- Option 1 : les fichiers pour la diffusion plein-texte sont issus des étapes du *Flux des travaux*, stockés comme *Fichiers publiables*.

    > **Rappel**
    >
    > - *Flux des travaux* > *Production*
    > - *Fichiers publiables*
    > - *Transférér le fichier* : ajouter les archives `zip` (contenant le fichier XML et les images associées).
    
    
    
    
      - *Sélectionner les fichiers*
    
      - Cocher l'option *Inclure tous les fichiers de toutes les étapes de production accessibles* pour afficher la liste des fichiers issus du *Flux des travaux*, étape *Production*.
    
      - Sélectionner les fichiers disponibles via le *Flux des travaux* pour la publication en plein-texte.
    


- Option 2 : charger les fichiers pour publication plein-texte.

  - *Modifier le fichier*
  - Suivre les étapes pour charger un fichier (*Transférer la soumission*, *Métadonnées*, *Terminer*)

- [important !] Attribuer, pour chaque fichier, les autorisations pour la diffusion plein texte :
  - *Approbation*
  - *Modalités de téléchargement* : *Accès libre*.



#### Constituer les chapitres

Passer dans l'onglet *Chapitres* afin d'ajouter les chapitres et affecter le.s format.s de publication.

- Ajouter un chapitre : *Ajouter un chapitre* ; a minima, saisir la métadonnée titre ;
- Sélectionner parmi les fichiers listés (formats) celui ou ceux correspondant au chapitre (`zip`, éventuellement PDF, epub…).



À partir de la même page, il est possible de prévisualiser la publication (*Aperçu*) et de la publier (*Publier*).



### OJS : procédure de chargement des fichiers

Ces étapes sont réalisées à l'étape *Publication* > onglet *Épreuves*.

Ajouter une épreuve :
- Étiquette : "HTML" ;
- Transférer le fichier

  - Sélectionner le type de document ("Texte de l'article" ou autre type de document) ;
  - Charger l'archive `zip` exporté depuis *XMLMind* puis cliquer sur *Continue*
- (facultatif) Éditer le nom du fichier soumis puis *Continue*
- *Terminer*



À partir de la même page, il est possible de prévisualiser la publication (*Aperçu*) et de programmer sa publication (*Calendrier de publication*).




#### Remarques 

**Quick submit plugin** : L'archive `.zip` peut être fournie lors de la soumission, compatible avec le `quick submit plugin`.



## Notes techniques

### Structure des archives soumises au plugin

Le plugin gère deux structures pour les fichiers soumis :

1. chargement d'un fichier xml + chargement une à une des images associées à l'article ;
2. chargement d'une archive au format `zip` contenant le fichier xml + les images associées.



Dans le second cas :

 - l'archive est décompressée dans le dossier du zip ;
 - le dossier décompressé est renommé avec l'id unique généré par ojs ;
 - le fichier xml est renommé avec ce même identifiant : `[id]/[id].xml` ; 
 - les urls des images sont de la forme `[idarticle]/zimage/[id1]/[id2][nom_image].[extension]`.



### Feuilles de transformations XSLT



#### TEI Commons Publishing to HTML

- Gestion des index

**Roadmap**

- Internationalisation des labels d'affichage



#### JATS Publishing 1.3 to HTML

**Roadmap**

- Internationalisation des labels d'affichage



#### Ajouter une feuille de transformation XSLT



### Interactivité de la page web (javascript)

Par défaut :

- Tables des matières cliquable ;
- Circulation depuis les appels de notes faire les notes et retour ;
- Illustrations :
  - circulation depuis les titres des illustrations vers les illustrations (panneau latéral) ;
- Tableaux, double mode de visualisation :
  - seul le titre est affiché, cliquer sur le titre pour afficher le contenu dans la fenêtre principale ;
  - cliquer sur l'îcone <span style="font-weight:bold;color:#0a84ff;">↗</span> pour ouvrir le tableau dans une fenêtre externe ;
- Références bibliographiques : navigation des liens de références courtes vers les références longues ;
- Index : listes dépliables des index, lien pour chaque occurrence avec retour au texte ;
- Dark mode : mode de lecture sombre.



**Roadmap**

- Figures : affichage des illustrations dans une fenêtre externe (visualiseur IIIF) ;
- Plusieurs jeux de notes ;
- Division de la fenêtre principale (texte et panneaux) redimensionnable ;



### Mise en forme des contenus (CSS)

– 
