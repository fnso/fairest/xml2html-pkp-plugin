<?php

use APP\pages\article\ArticleHandler;
use APP\core\Services;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\UnixVisibility\PortableVisibilityConverter;
use PKP\config\Config;
use PKP\file\FileManager;
use PKP\plugins\Hook;

class ArticleImageDataHandler extends ArticleHandler
{

    private $fs;

    public function __construct()
    {
        parent::__construct();
        $this->fs = ImageUtils::buildFileSystem($this);
    }

    /**
     * Image from zip archive galley file
     * @param array $args
     * @param APP\core\Request $request
     * @return mixed
     */
    public function zimage(array $args, APP\core\Request $request)
    {

        $galleyId = (int)$args[1];
        $imgName = $args[2];
        $galley = NULL;
         foreach ($this->publication->getData('galleys') as $g) {
             if($galleyId == $g->getId()){
                 $galley = $g;
             }
         }
         if($galley != NULL){
             $file = $galley->getFile();
             $filename = Services::get('file')
                 ->formatFilename($file->getData('path'), $file->getLocalizedData('path'));
             $pathInfo = pathinfo($filename);
             $imagePath = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'] . DIRECTORY_SEPARATOR . $imgName;
             ImageUtils::downloadImage($this->fs, $imagePath, $imgName);
         }
        return NULL;

    }


}
