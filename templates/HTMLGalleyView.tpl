
<!DOCTYPE html>
<html lang="{$currentLocale|replace:"_":"-"}" xml:lang="{$currentLocale|replace:"_":"-"}">
    <head>
        <meta charset="{$defaultCharset|escape}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            {$currentContext->getLocalizedName()}
        </title>

        <link rel="stylesheet" type="text/css" href="{$assetsPath}styles/xml_2_html.css">
        <script>
            let lang = "{$currentLocale}"
        </script>
        <script src="{$assetsPath}javascript/xml_2_html.js"></script>
    </head>
    <body>
    <div class="page">
        {$text}
    </div>
</body>
</html>


