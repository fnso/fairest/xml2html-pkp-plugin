
<script>
    $(function() {ldelim}
        // Attach the form handler.
        $('#xml2htmlSettingsForm').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
        {rdelim});
</script>

<form class="pkp_form" id="xml2htmlSettingsForm" method="post" action="{url router=$smarty.const.ROUTE_COMPONENT op="manage" category="generic" plugin=$pluginName verb="settings" save=true}">
    <div id="xml2htmlSettings">
        {csrf}
        {fbvFormSection label="plugins.generic.xml2html.format"}
        <select name="format">
            <option value="JATS" {if $format=='JATS'}
                    selected
                    {/if}>JATS</option>
            <option value="TEI" {if $format=='TEI'}
            selected
                    {/if}>TEI</option>
        </select>
        {/fbvFormSection}
        {fbvFormButtons submitText="common.save"}
    </div>
</form>
